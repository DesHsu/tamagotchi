package desmondhsu.tamagotchi;

import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * Created by Des on 09-Feb-16.
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class SmileyTest {
    Pet myPet;

    @Before
    public void setup(){

    }


    @Test
    public void StartingOverAge(){
        myPet= new Smiley(Smiley.MAX_AGE*5,0,0,0,0,0,0);
        myPet.update(0);
        assert(!myPet.vitality);
    }

    @Test
    public void OverTime(){
        myPet= new Smiley(0,0,0,0,0,0,0);
        myPet.update(System.currentTimeMillis());
        assert(!myPet.vitality);
    }

    @Test
    public void SelfWakeUp(){
        myPet= new Smiley(0,0,0,0,0,0,0);
        myPet.update(Smiley.SLEEP_PERIOD+1);
        assert(!myPet.asleep);
    }

    @Test
    public void ForcedWakeUp(){
        myPet= new Smiley(0,0,0,0,0,0,0);
        myPet.update(Smiley.SLEEP_PERIOD*2+1);
        assert(!myPet.asleep);
        myPet.update(Smiley.SLEEP_PERIOD*2+100001);
        assert(myPet.asleep);
    }


}