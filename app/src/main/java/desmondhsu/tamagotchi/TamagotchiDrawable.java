package desmondhsu.tamagotchi;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RectShape;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by Des on 09-Feb-16.
 */
public class TamagotchiDrawable extends View {
    private int canvasHeight;
    private int canvasWidth;
    private Pet myPet;
    private PetView myPetView;
    private GestureDetector gestureDetector;

    public TamagotchiDrawable(Context context, Pet myPet, PetView myPetView) {
        super(context);
        this.myPet=myPet;
        this.myPetView=myPetView;
        //Set background
        ShapeDrawable background=new ShapeDrawable(new RectShape());
        background.getPaint().setColor(Color.parseColor("#0099cc"));
        background.setBounds(100, 250, 100, 250);
        setBackground(background);

        class mListener extends GestureDetector.SimpleOnGestureListener {
            @Override
            public boolean onDown(MotionEvent e) {
                return true;
            }
        }
        gestureDetector = new GestureDetector(context, new mListener());

    }


    @Override
    protected void onSizeChanged(int canvasHeight, int canvasWidth, int oldw, int oldh) {
        this.canvasHeight= canvasHeight;
        this.canvasWidth = canvasWidth;
        super.onSizeChanged(canvasHeight, canvasWidth, oldw, oldh);
    }

    protected void onDraw(Canvas canvas) {
        myPet.update(System.currentTimeMillis());
        myPetView.drawPetGraphics(canvas, canvasHeight, canvasWidth);
        invalidate();
    }

    float lastXPosition=0;
    float lastYPosition=0;
    float MIN_SWIPE_DISTANCE=200;
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        boolean result = gestureDetector.onTouchEvent(event);
        if (!result) {
            if (event.getAction() == MotionEvent.ACTION_UP) {

                if(Math.abs(lastYPosition-event.getY()+1000)>MIN_SWIPE_DISTANCE){
                    if(lastYPosition-event.getY()+1000>0){
                        myPet.feed();
                    }else{
                        myPet.clean();
                    }
                }else
                if(Math.abs(lastXPosition-event.getX()+500)>MIN_SWIPE_DISTANCE){
                    if(lastXPosition-event.getX()+500>0){
                        myPet.putToSleep();
                        Log.d("Movement", "Sleep");
                    }else{
                        myPet.wakeUp();
                        Log.d("Movement", "wake");
                    }

                }
                result = true;
            }
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                lastYPosition=event.getY();
                lastXPosition=event.getX();
                result = true;
            }
        }
        return result;
    }
}