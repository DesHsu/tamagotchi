package desmondhsu.tamagotchi;

/**
 * Created by Des on 09-Feb-16.
 */
public class Pet {

    public static final int MAX_AGE= 604800000;            //dies when max age is reached; 604800000= 7 days
    public static final int BIRTH_AGE= 60000;              //come out of egg when birth age is reached; 60000=10min
    public static final int TEEN_AGE= 86400000;            //teen age ; 86400000= 1 day
    public static final int ADULT_AGE= 259200000;          //adult age ; 259200000= 3 days
    public static final int ELDER_AGE= 518400000;          //elder age ; 518400000= 6 days
    public static final int MAX_POOP_LEVEL= 10800000;      //poop when max level is reached; 10800000= 3 hours
    public static final int MAX_HUNGER_LEVEL= 43200000;    //dies when max level is reached; 43200000=12 hours
    public static final int MAX_UNSANITARY_LEVEL= 54000000; //dies when max level is reached; 54000000 equivalent = 1 poop for 15 hours. Or 2 poops for 7.5 hours
    public static final int SLEEP_PERIOD= 14400000;         //goes to sleep in ever second of the period



    long age;
    int poopLevel;
    int hungerLevel;
    int unsanitaryLevel;
    int poopCount;
    long lastSystemTime;
    boolean vitality= true;
    boolean asleep=false;
    int sleepingTime;
    int awakeTime;


    Pet(){
    }

    Pet(long age, int poopLevel, int hungerLevel, int unsanitaryLevel, int poopCount, int sleepingTime, long lastSystemTime){
        this.age=age;
        this.poopLevel=poopLevel;
        this.hungerLevel=hungerLevel;
        this.unsanitaryLevel=unsanitaryLevel;
        this.poopCount=poopCount;
        this.lastSystemTime=lastSystemTime;
        this.sleepingTime=sleepingTime;
    }

    public void update(long systemTime){
        long timeLapsed=systemTime-lastSystemTime;

        //Age, digestion, and sleeping pattern laps with time
        age+=timeLapsed;
        poopLevel+=timeLapsed;
        hungerLevel+=timeLapsed;
        sleepingTime-=timeLapsed;

        //when pet is being forced awake, there is 10 seconds before it goes back to sleep
        awakeTime-=timeLapsed;

        //natural sleep
        if((age/SLEEP_PERIOD)%3==1&&awakeTime<=0&&!asleep){
            asleep=true;
            sleepingTime=SLEEP_PERIOD;
        }
        //natural wake up
        if(sleepingTime<=0){
            asleep=false;
        }



        unsanitaryLevel+=poopCount*timeLapsed;

        //Pet does not poop when sleeping
        if(!asleep) {
            poopCount += poopLevel / MAX_POOP_LEVEL;
            poopLevel = poopLevel % MAX_POOP_LEVEL;
        }

        lastSystemTime=systemTime;


        if(age>=MAX_AGE||hungerLevel>=MAX_HUNGER_LEVEL||unsanitaryLevel>=MAX_UNSANITARY_LEVEL){
            vitality=false;
        }



    }

    public void feed(){
        hungerLevel=0;
    }

    public void clean(){
        poopCount=0;
        unsanitaryLevel=0;

        if(!vitality){
            reset();
        }
    }

    public void reset(){
        age=0;
        hungerLevel=0;
        unsanitaryLevel=0;
        poopLevel=0;
        poopCount=0;
        lastSystemTime=System.currentTimeMillis();
        vitality=true;
    }


    public void putToSleep(){
        asleep=true;
        sleepingTime=SLEEP_PERIOD;
    }


    public void wakeUp(){
        asleep=false;
        awakeTime=10000;
    }
}
