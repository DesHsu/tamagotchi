package desmondhsu.tamagotchi;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

import java.util.ArrayList;

/**
 * Created by Des on 09-Feb-16.
 */
public class PetView {



    Pet myPet;
    ArrayList<Integer> poopDisplayX= new ArrayList<Integer>();
    ArrayList<Integer> poopDisplayY= new ArrayList<Integer>();
    Context context;
    PetView(Pet myPet, Context context){
        this.context=context;
        this.myPet=myPet;
    }

    public void drawPetGraphics(Canvas canvas, int canvasHeight, int canvasWidth){
        Drawable myDrawable;
        if(myPet.vitality) {

            if(myPet.asleep){
                myDrawable = context.getResources().getDrawable(R.mipmap.asleep);
            }else if (myPet.hungerLevel > myPet.MAX_HUNGER_LEVEL / 2 && myPet.unsanitaryLevel > myPet.MAX_UNSANITARY_LEVEL / 2) {
                myDrawable = context.getResources().getDrawable(R.mipmap.sad);
            } else if (myPet.hungerLevel > myPet.MAX_HUNGER_LEVEL / 2) {
                myDrawable = context.getResources().getDrawable(R.mipmap.hungry);
            } else if (myPet.poopCount >  2) {
                myDrawable = context.getResources().getDrawable(R.mipmap.unsanitary);
            } else {
                myDrawable = context.getResources().getDrawable(R.mipmap.ok);
            }
            Paint textPaint = new Paint();
            textPaint.setTextSize(50);

            canvas.drawText("Age: " + myPet.age / 1000 + "/" + myPet.MAX_AGE/1000, 10, 100, textPaint);
            canvas.drawText("Hunger Level: " + myPet.hungerLevel / 1000+"/"+myPet.MAX_HUNGER_LEVEL/1000, 10, 150, textPaint);
            canvas.drawText("Unsanitary Level: " + myPet.unsanitaryLevel/1000+"/"+myPet.MAX_UNSANITARY_LEVEL/1000, 10, 200, textPaint);
            canvas.drawText("Poop count: " + myPet.poopCount+"( "+myPet.poopLevel/1000+"/"+myPet.MAX_POOP_LEVEL/1000+")", 10, 250, textPaint);

            canvas.drawText("SWIPE UP: to feed", 10, canvasWidth-50, textPaint);
            canvas.drawText("SWIPE DOWN: to clean", 10, canvasWidth-100, textPaint);

            //drawing poops
            Drawable poopDrawable= context.getResources().getDrawable(R.mipmap.poop);
            Bitmap poopImage = ((BitmapDrawable) poopDrawable).getBitmap();
            for(int i=0;i<=myPet.poopCount-poopDisplayX.size();i++){    //add poop
                poopDisplayX.add((int)(Math.random()*(canvasWidth-600)+300));
                poopDisplayY.add((int)(Math.random()*(canvasHeight-600)+300));
            }
            for(int i=0;i<Math.min(poopDisplayX.size(),myPet.poopCount);i++){ //draw them
                canvas.drawBitmap(poopImage,poopDisplayY.get(i),poopDisplayX.get(i), new Paint());
            }
        }
        else
        {
            myDrawable = context.getResources().getDrawable(R.mipmap.dead);

        }
        Bitmap petImage = ((BitmapDrawable) myDrawable).getBitmap();
        canvas.drawBitmap(petImage, canvasHeight / 2, canvasWidth / 2, new Paint());





    }

}
