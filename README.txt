Features:
	Swipe up to feed the pet
	Swipe down to clean its living environment (If the pet is deceased, swipe down to respawn)
	Swipe left to put the pet to sleep
	Swipe right to wake up the pet
	
	Capable of going to sleep on its own, losing health from hunger and pooping on its own without prompting
	Pet does not deficate until it is awake
	Can die of inhygenic environment
	While the pet should be sleeping, force wake up will only last 10 seconds
	Can die of old age
	
Status:
	Sleeping
	Dead
	OK < :) >
	Hungry < :( >
	Requires cleaning < >:| >
	Requires much attention < :'( >

How to import Tamagotchi:
	Open Android Studio V1.4+
	File > Open > Select "Path/to/project"
	
How to compile/run Tamagotchi
	Enable Android developer option
	Connect Android phone to computer
	Run application
	
Design pattern:
	Model-View-Controller: increase maintainability and testability

Classes:
	FullscreenActivity.java (Android Activity)
	Pet.java
	Smiley.java (Act as MODEL, contains game logic)
	PetView.java (Act as VIEW, display game)
	TamagotchiDrawable.java (Act as CONTROLLER, provides canvas for VIEW)
	
Data:
	Stored in SharedPreference
	
How to run tests:
	Import JUnit
	Run Program > Run > SmileyTest(JUnit)
	

